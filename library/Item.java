package library;

public class Item {

    String title;
    String genre;
    public Item(){
        this.title = "Unknown";
        this.genre = "Unknown";
    }

    

    public Item(String title, String genre){

        this.title = title;
        this.genre = genre;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    



    
}