package library;

public class CD extends Item {
    
    String musician;


    public CD(String title, String genre, String musician) {
        super(title, genre);
        this.musician = musician;

    }

    public String getMusician() {
        return musician;
    }

    public void setMusician(String musician) {
        this.musician = musician;
    }

  

    

    

}