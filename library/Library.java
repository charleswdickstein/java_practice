package library;


public class Library {
    
    Book[] arrBooks;
    CD[] arrCDs;
    DVD[] arrDVDs;
    Periodical[] arrPeriodicals;

    
    public boolean doesItemExist(Item item, Item[] arrItems) {
        String itemTitle = item.getTitle();
        for (int i = 0; i < arrItems.length; i++){
            if (arrItems[i].getTitle().equals(itemTitle)){
                System.out.println("Item found");
                return true;
            }
        }
        return false;

    }

    public Item[] addItemToArr(Item item, Item[] arrItems) {

        int itemCount = getNullSlot(arrItems);

        if (itemCount != 6){
            arrItems[itemCount] = item;
        }

        return arrItems;

        
    }



    public Item[] removeItemFromArr(Item item, Item[] arrItems) {

        String itemTitle = item.getTitle();

        for (int i = 0; i < arrItems.length; i++){
            if (arrItems[i].getTitle().equals(itemTitle)){
                System.out.println("Item found");
                arrItems[i] = null;
                return arrItems;
            }
        }
        System.out.println("Item not found");
        return arrItems;
    }

    public int getNullSlot(Item[] arrItems){

        for (int i = 0; i < arrItems.length; i++){

            if (arrItems[i] == null){
                return i;
            }
        }
        return 6;




    }

    public Library() {
        this.arrBooks = new Book[6];
        this.arrCDs = new CD[6];
        this.arrDVDs = new DVD[6];
        this.arrPeriodicals = new Periodical[6];
    }


}