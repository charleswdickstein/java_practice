package ch10;

public class TestStrings {

    public static void main(){

        String s1 = "example.doc";
        String s2 = "example.bak";

        System.out.println(s1 == s2);
        System.out.println(s1.compareTo(s2));

    }
}