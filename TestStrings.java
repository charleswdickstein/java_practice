public class TestStrings{

    public static void main(String[] args) {
        String s1 = "example.doc";
        String s2 = "example.bak";
        System.out.println(s1.equals(s2));
        System.out.println(s1.compareTo(s2) > 0);

        String sentence = "the quick brown fox swallowed down the lazy chicken";
        int count = 0;
        sentence = sentence.replaceAll("\\s+","");
        for (int i = 0; i < sentence.length()-2;){
            System.out.println(sentence.substring(i, i+2));
            if ("ow".equals(sentence.substring(i, i+2))){
                count +=1;
            }
            i+=2;
        }
        System.out.println("count " + count);

    }



}