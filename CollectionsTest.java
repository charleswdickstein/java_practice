import java.util.Collections;
import java.util.HashSet;

import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

public class CollectionsTest {

    public static void main(String[] args) throws DodgyNameException {
        TreeSet<Account> accounts = new TreeSet<Account>(new 
        AccountComparator());;
        Account a1 = new Account("ABC", 30.0);
        Account a2 = new Account("DEF", 20.0);
        Account a3 = new Account("GHI", 40.0);
        accounts.add(a2);
        accounts.add(a1);

        accounts.add(a3);

        Iterator<Account> it = accounts.iterator();
        //Collections.sort((List<T>) accounts, new AccountComparator());
        while (it.hasNext()){
         
            Account curr = it.next();
            System.out.println(curr.getName() + " " + curr.getBalance());
            curr.addInterest();
            System.out.println("new balance " + curr.getBalance());
            System.out.println();
        }

        // 6
        System.err.println("6.");
        for (Account curr : accounts){
            System.out.println(curr.getName() + " " + curr.getBalance());
            curr.addInterest();
            System.out.println("new balance " + curr.getBalance());
            System.out.println();

        }
        System.err.println("7.");
        accounts.forEach(curr -> {
            System.out.println(curr.getName() + " " + curr.getBalance());
            curr.addInterest();
            System.out.println("new balance " + curr.getBalance());
            System.out.println();
        });

        
        
    }



    


}