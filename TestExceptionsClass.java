public class TestExceptionsClass{

    public static void main(String[] args) {
       

        try {
            Account[] arrOfAccounts = {new Account("dave", 10.0), new Account("John", 10.0)};
            arrOfAccounts[0].setName("alex");
            System.out.println("seeting name to alex worked");
            arrOfAccounts[0].setName("Fingers");
        }
        catch (DodgyNameException d){
            System.out.println(d.toString());
        }


    }

}