public class Account extends DodgyNameException implements Detailable{

    double balance;
    String name;
    public static double interestRate;

    public String getName() {
        return name;
    }

    public void setName(String name) throws DodgyNameException{
        if (name.equals("Fingers")){
            throw new DodgyNameException();
        }
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void addInterest(){
        this.balance += (this.balance*0.1);
    }

    public boolean withdraw(double amount){
        double new_balance = this.balance - amount;
        if ( new_balance >= 0.0){
            this.setBalance(new_balance);
            return true;
        }
        return false;

    }

    public boolean withdraw(){
        return withdraw(20.0);

    }

    public Account(String name, double balance) throws DodgyNameException{
        this.balance = balance;
        if (name.equals("Fingers")){
            throw new DodgyNameException();
        }
        this.name = name;
    }

    public Account() throws DodgyNameException{
       this("Jon Doe", 5000.0);
       
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    @Override
    public String getDetails() {
        // TODO Auto-generated method stub
        return " User: " + this.name + " Balance: " + this.balance;
    }
    
    
}